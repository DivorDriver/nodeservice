﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace StartNodeFile
{
    public partial class StartNodeFile : ServiceBase
    { 
        public static Timer timer1 = null;
        public StartNodeFile()
        {
            InitializeComponent();
           
        }

        protected override void OnStart(string[] args)
        {
            StartNodeFile.timer1 = new Timer();
            StartNodeFile.timer1.Interval = 5000; //every 30 secs
            StartNodeFile.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            StartNodeFile.timer1.Enabled = true;
        }

        protected override void OnStop()
        {
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            NodeFile.TickTimer();
        }  
        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            try
            {
                NodeFile.TickTimer();
               
            }
            catch (Exception ex)
            {
              
            }
        }
    }
}
